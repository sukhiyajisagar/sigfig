**Features Supported by Framework :**

1. Parallel execution with multiple tests/browsers.
2. Parallel log4j logging based on thread.
3. Integrated with Jenkins effortlessly.
4. Maven based project.
5. Extent reporting which works in parallel runs.
6. TestNG Listeners for capturing screenshots when testcase fails.

**Other Details:** 

An simple robust framework for selenium build with java. It will generate the extent report with the screenshots appended to the report.
Url, Username, Password, Browser can be given easily from data.properties file.

**How to use this framework?**

1. Clone the repository to your workspace.
2. By default all testcase is going to run in Chrome browser parallel. If you want to change browser setting then open data.properties file under the src/main/resources folder. And then change value for browser key. 
3. Run the testng.xml file (if testNG plugin installed in your IDE). You can even run as mvn test which will trigger the testng.xml
4. Once all testcase executed, generated report can be found inside reports -> index.html.
5. Generated log files can be found inside logs folder.  

**Note:**

- src/test/java - contains testcases
- src/main/java - contains page objects
- src/main/java - contains resources ( like ltestNG istener, log4j configuration, extent report, chrome driver exe, property file, base class )