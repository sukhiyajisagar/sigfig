package testcases;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.aventstack.extentreports.Status;

import commonUtills.Base;
import pages.DefaultPage;
import pages.HomePage;
import pages.LoginPage;

public class ValidUserLogin extends Base {
	public WebDriver driver;
	static final Logger log = LogManager.getLogger(ValidUserLogin.class.getName());

	@BeforeTest
	public void initialize() throws IOException {
		driver = initializeDriver();
		ThreadContext.put("ROUTINGKEY", ValidUserLogin.class.getName());
		log.debug("Opening chrome browser");
	}

	@Test
	public void validUserLogin() throws InterruptedException {
		ThreadContext.put("ROUTINGKEY", ValidUserLogin.class.getName());
		driver.get(prop.getProperty("url"));
		log.debug("SigFig website launched");
		extentTest.get().log(Status.INFO, "SigFig site launched");
		DefaultPage defaultPageObj = new DefaultPage(driver);
		defaultPageObj.getLoginButtonWebelement().click();
		log.debug("Login button of default page clicked");
		extentTest.get().log(Status.INFO, "User clicked on login button of default page");
		LoginPage loginPageObj = new LoginPage(driver);
		loginPageObj.getUsername().sendKeys(prop.getProperty("username"));
		log.debug("Username inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("username") + " in username");
		loginPageObj.getPassword().sendKeys(prop.getProperty("password"));
		log.debug("Password inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("password") + " in password");
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertTrue(loginPageObj.getRememberMe().getAttribute("aria-checked").contains("false"),
				"Checkbox unchecked");
		log.debug("Remember me checkbox by default unchecked");
		loginPageObj.getLoginButton().click();
		log.debug("Login button clicked");
		HomePage homePage = new HomePage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(homePage.getAddPortfolioButton()));
		softAssert.assertEquals(driver.getCurrentUrl(), "https://www.sigfig.com/f/#/holdings");
		softAssert.assertAll();
		extentTest.get().log(Status.INFO, "Remember me checkbox by default unchecked");
		extentTest.get().log(Status.INFO, "User clicked on login button to sign-in");
		log.debug("Successfully logged in");
		extentTest.get().log(Status.INFO, "User successfully logged in");
	}

	@AfterTest
	public void teardown() {
		ThreadContext.put("ROUTINGKEY", ValidUserLogin.class.getName());
		driver.close();
		log.debug("Closing browser");
	}

}
