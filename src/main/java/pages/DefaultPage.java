package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DefaultPage {

	WebDriver driver;

	public DefaultPage(WebDriver driver) {
		this.driver = driver;
	}

	private By loginButton = By.linkText("Log In");

	public WebElement getLoginButtonWebelement() {
		return driver.findElement(loginButton);
	}
}
